﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioList : MonoBehaviour {

	public AudioSource selectCharSound;
	public AudioSource playSound;
	public AudioSource jumpSound;
	public AudioSource jumpHighSound;
	public AudioSource gotSpikeSound;
	public AudioSource fallSound;

}
