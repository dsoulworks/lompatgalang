﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JumpForce : MonoBehaviour {

	public PlatformSpawner platformScript;
	public Score scoreScript;
	public AudioList audioScript;
	public GameObject[] charButtons;
	public GameObject[] lockedCharButtons;
	public GameObject pBody;
	public GameObject pRig;
	public GameObject pFace;
	public Sprite[] faceSprite;
	public GameObject boostCharacter;
	public GameObject boostBang;
	public GameObject highJumpCharacter;
	public GameObject invisibleCharacter;
	public Canvas mainMenuCanvas;
	public GameObject playButton;
	public Animation playButtonFx;
	public GameObject hitPuff;
	public float thrust;
	public Rigidbody rb;
	private GameObject cam;
	private bool isPlaying = false;
	public Text nameText;
	public bool increaseScore = false;
	public bool playerOuch = false;
	public bool isJumpHigh = false;
	public bool isInvisible = false;
	private int id = 0;

	void Start () {

		for (int i = 0; i < lockedCharButtons.Length; i++) {
			lockedCharButtons [i].SetActive (true);
		}

		mainMenu ();
		rb = GetComponent<Rigidbody>();
		cam = GameObject.FindGameObjectWithTag ("MainCamera");
	}

	void LateUpdate () {

		//TODO: If vertical speed > 0, hide the player's box collider so it could move pass the platform.
		//		Else, show the player's box collider so when the player falls it could bounch on the platform.
		if (rb.velocity.y > 0) {
			gameObject.GetComponent<BoxCollider> ().enabled = false;
			if (increaseScore && isPlaying) {
				scoreScript.scoreVal++;
				scoreScript.SetScore ();
			}
		} else {
			gameObject.GetComponent<BoxCollider> ().enabled = true;
		}

		//TODO: Control the boost effect and the disappearance of the boost character (Chuck), when the player step on Chuck.
		if (boostCharacter.activeSelf) {
			if (rb.velocity.y > 5f && rb.velocity.y < 6f) {
				boostBang.GetComponent<Animation> ().Play ();
			}
			if (rb.velocity.y > 0 && rb.velocity.y < 2f) {
				boostCharacter.SetActive (false);
			}
		}

		//TODO: Control the camera following
		if (rb.velocity.y > -5f && rb.velocity.y < 5f) {
			CamUnfollow ();
		} else if (rb.velocity.y < -30f) {
			CamUnfollow ();
		} else {
			CamFollow ();
		}

		if (rb.velocity.y < -40f) {
			//SceneManager.LoadScene("scene_0");
			//RestartGame ();
			audioScript.fallSound.Play();
			mainMenu ();
			rb.velocity = new Vector3 (0, 0, 0);
		}

		if (playerOuch) {
			audioScript.gotSpikeSound.Play();
			hitPuff.GetComponent<Animation> ().Play ();
			//RestartGame ();
			mainMenu ();
			playerOuch = false;
		}

	}

	public void JumpUp () {

		audioScript.jumpSound.Play ();
		float factor = 1;
		if (isJumpHigh)
			factor = 1.5f;
		
		rb.AddForce(transform.up * thrust * factor);

	}

	private void CamFollow () {

		cam.transform.position = new Vector3 (cam.transform.position.x, transform.position.y, cam.transform.position.z);

	}

	private void CamUnfollow () {

		cam.transform.position = new Vector3 (cam.transform.position.x, cam.transform.position.y, cam.transform.position.z);

	}

	public void mainMenu () {

		isPlaying = false;
		pBody.SetActive (false);
		pRig.SetActive (false);
		boostCharacter.SetActive (false);
		highJumpCharacter.SetActive (false);
		gameObject.GetComponent<Rigidbody> ().useGravity = false;
		mainMenuCanvas.enabled = true;
		playButton.SetActive (false);

		int hiScore = PlayerPrefs.GetInt ("hiScorePref");
		if (hiScore >= 3000) {
			charButtons [1].SetActive (true);
			lockedCharButtons [0].SetActive (false);
			charButtons [2].SetActive (true);
			lockedCharButtons [1].SetActive (false);
			charButtons [3].SetActive (true);
			lockedCharButtons [2].SetActive (false);
			charButtons [4].SetActive (true);
			lockedCharButtons [3].SetActive (false);
		} else if (hiScore >= 2500) {
			charButtons [1].SetActive (true);
			lockedCharButtons [0].SetActive (false);
			charButtons [2].SetActive (true);
			lockedCharButtons [1].SetActive (false);
			charButtons [3].SetActive (true);
			lockedCharButtons [2].SetActive (false);
		} else if (hiScore >= 1500) {
			charButtons [1].SetActive (true);
			lockedCharButtons [0].SetActive (false);
			charButtons [2].SetActive (true);
			lockedCharButtons [1].SetActive (false);
		} else if (hiScore >= 1000) {
			charButtons [1].SetActive (true);
			lockedCharButtons [0].SetActive (false);
		}

	}

	public void RestartGame () {

		audioScript.playSound.Play ();
		isPlaying = true;
		playerOuch = false;

		pBody.SetActive (true);
		pRig.SetActive (true);
		highJumpCharacter.SetActive (false);
		invisibleCharacter.SetActive (false);
		isJumpHigh = false;
		isInvisible = false;

		if (id == 0) {
			pFace.GetComponent<SpriteRenderer> ().sprite = faceSprite [0];
		} else if (id == 1) {
			pFace.GetComponent<SpriteRenderer> ().sprite = faceSprite [1];
		} else if (id == 2) {
			pFace.GetComponent<SpriteRenderer> ().sprite = faceSprite [2];
		} else if (id == 3) {
			pBody.SetActive (false);
			pRig.SetActive (false);
			highJumpCharacter.SetActive (true);
			isJumpHigh = true;
		} else if (id == 4) {
			pBody.SetActive (false);
			pRig.SetActive (false);
			invisibleCharacter.SetActive (true);
			isInvisible = true;
		}

		gameObject.GetComponent<Rigidbody> ().useGravity = true;
		playButton.SetActive (true);

		transform.position = new Vector3 (0, 0, 0);
		gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
		platformScript.InitPlatform ();
		scoreScript.InitScore ();
		playButton.SetActive (false);
		mainMenuCanvas.enabled = false;

	}

	public void SelectHumanCharacter ( int idx) {

		audioScript.selectCharSound.Play ();
		id = idx;
		playButton.SetActive (true);
		if (id == 0) {
			nameText.text = "GALANG";
		} else if (id == 1) {
			nameText.text = "AJWA";
		} else if (id == 2) {
			nameText.text = "ZEEYA";
		} else if (id == 3) {
			nameText.text = "RED";
		} else if (id == 4) {
			nameText.text = "DAVE";
		}
		charButtons [id].GetComponent<Animation> ().Play ();
		playButtonFx.Play ();

	}

}
