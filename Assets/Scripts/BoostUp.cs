﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostUp : MonoBehaviour {

	public float thrust = 1000f;

	void Start () {
		
	}

	void Update () {
		
	}

	void OnCollisionEnter (Collision col) {

		if (col.gameObject.tag == "Player") {
			col.gameObject.GetComponent<JumpForce> ().audioScript.jumpHighSound.Play ();
			col.gameObject.GetComponent<JumpForce> ().increaseScore = true;
			col.gameObject.GetComponent<JumpForce> ().thrust = thrust;
			col.gameObject.GetComponent<JumpForce> ().JumpUp ();
			col.gameObject.GetComponent<JumpForce> ().boostCharacter.SetActive (true);
			//Debug.Log("BOOST!");
			gameObject.SetActive (false);
		}

	}

}
