﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

	public float speed = 10f;

	void Update () {

		//TODO: Control movement by tilting mobile device
		transform.Translate(Input.acceleration.x * -speed * Time.deltaTime, 0, 0);

		#if UNITY_EDITOR
			float translation = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			transform.Translate(-translation, 0, 0);
		#endif

		//TODO: If player moves pass the left side of screen, it will appear on the right side of the screen
		if (transform.position.x < -3.5f) {
			transform.position = new Vector3 (3.5f, transform.position.y, transform.position.z);
		}

		//TODO: If player moves pass the right side of screen, it will appear on the left side of the screen
		if (transform.position.x > 3.5f) {
			transform.position = new Vector3 (-3.5f, transform.position.y, transform.position.z);
		}
		
	}
}
