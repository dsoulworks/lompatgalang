﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUp : MonoBehaviour {

	public float thrust = 500f;
	public float speed = 5f;
	public bool isFall = false;
	public bool isMove = false;
	public bool isSpike = false;
	private bool moveRight = true;
	public int touch = 0;

	void Start () {
		
	}

	void Update () {

		if (isMove) {
			if (moveRight) {
				transform.position = new Vector3 (transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
			} else {
				transform.position = new Vector3 (transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
			}

			if (transform.position.x < -2f) {
				moveRight = true;
			}

			if (transform.position.x > 2f) {
				moveRight = false;
			}
		}

	}
	
	void OnCollisionEnter (Collision col) {

		if (col.gameObject.tag == "Player") {
			touch++;
			if (touch > 1) {
				col.gameObject.GetComponent<JumpForce> ().increaseScore = false;
			} else {
				col.gameObject.GetComponent<JumpForce> ().increaseScore = true;
			}
			col.gameObject.GetComponent<JumpForce> ().thrust = thrust;
			col.gameObject.GetComponent<JumpForce> ().JumpUp ();

			if (isFall) {
				gameObject.GetComponent<Rigidbody> ().mass = 10f;
				gameObject.GetComponent<Rigidbody> ().useGravity = true;
			}

			if (isSpike) {
				if (!col.gameObject.GetComponent<JumpForce> ().isInvisible) {
					col.gameObject.GetComponent<JumpForce> ().playerOuch = true;
				} else {
					col.gameObject.GetComponent<JumpForce> ().thrust = thrust * 75f;
					col.gameObject.GetComponent<JumpForce> ().JumpUp ();
				}
			}
		}

		if (col.gameObject.tag == "Platform") {
			touch = 0;
			col.gameObject.SetActive (false);
		}

	}

}
