﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public Text scoreText;
	public int scoreVal;
	private int hiScore;

	void Start () {
		
		//PlayerPrefs.SetInt ("hiScorePref", 0);
		InitScore ();

	}

	public void SetScore () {

		if (scoreVal > hiScore) {
			PlayerPrefs.SetInt ("hiScorePref", scoreVal);
			hiScore = PlayerPrefs.GetInt ("hiScorePref");
		}
		scoreText.text = scoreVal.ToString () + " Hi:" + hiScore;
		
	}

	public void InitScore () {

		scoreVal = 0;
		hiScore = PlayerPrefs.GetInt ("hiScorePref");
		scoreText.text = scoreVal.ToString () + " Hi:" + hiScore;

	}

}
