﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

	public GameObject platformBase;
	public GameObject chuck;
	public GameObject platformChuck;
	public GameObject[] platformRegularPool;
	public GameObject[] platformFallPool;
	public GameObject[] platformMovePool;
	public GameObject[] platformSpikePool;
	public int poolSize;
	public GameObject platformRegularPrefab;
	public GameObject platformFallPrefab;
	public GameObject platformMovePrefab;
	public GameObject platformSpikePrefab;
	public float platformHeight = 0;
	private bool spikeAppeared = false;

	void Start () {

		platformRegularPool = new GameObject[poolSize];
		platformFallPool = new GameObject[poolSize];
		platformMovePool = new GameObject[poolSize];
		platformSpikePool = new GameObject[poolSize];

		for (int i = 0; i < poolSize; i++) {
			GameObject pfRegular = (GameObject)Instantiate(platformRegularPrefab, transform.position, transform.rotation);
			GameObject pfFall = (GameObject)Instantiate(platformFallPrefab, transform.position, transform.rotation);
			GameObject pfMove = (GameObject)Instantiate(platformMovePrefab, transform.position, transform.rotation);
			GameObject pfSpike = (GameObject)Instantiate(platformSpikePrefab, transform.position, transform.rotation);
			platformRegularPool [i] = pfRegular;
			platformFallPool [i] = pfFall;
			platformMovePool [i] = pfMove;
			platformSpikePool [i] = pfSpike;
		}

		InitPlatform ();
			
	}

	void Update () {

		int ranVal = 0;
		if (!spikeAppeared) {
			ranVal = Random.Range (0, 16);
		} else {
			ranVal = Random.Range (0, 14);
		}

		float ranXpos = Random.Range (-2f, 2f);

		if (ranVal < 5) {
			for (int i = 0; i < poolSize; i++) {
				if (!platformRegularPool [i].activeSelf) {
					platformRegularPool [i].SetActive (true);
					platformHeight += 2f;
					platformRegularPool [i].transform.position = new Vector3 (ranXpos, platformHeight, 0);
					spikeAppeared = false;
					break;
				}
			}
		} else if (ranVal >= 5 && ranVal < 10) {
			for (int i = 0; i < poolSize; i++) {
				if (!platformFallPool [i].activeSelf) {
					platformFallPool [i].SetActive (true);
					platformHeight += 2f;
					platformFallPool [i].GetComponent<Rigidbody> ().useGravity = false;
					platformFallPool[i].GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
					//platformFallPool[i].transform.rotation = Quaternion.Euler(0, 0, 0);
					platformFallPool [i].transform.position = new Vector3 (ranXpos, platformHeight, 0);
					spikeAppeared = false;
					break;
				}
			}
		} else if (ranVal >= 10 && ranVal < 14) {
			for (int i = 0; i < poolSize; i++) {
				if (!platformMovePool [i].activeSelf) {
					platformMovePool [i].SetActive (true);
					platformHeight += 2f;
					platformMovePool [i].transform.position = new Vector3 (ranXpos, platformHeight, 0);
					spikeAppeared = false;
					break;
				}
			}
		} else if (ranVal >= 14) {
			if (!chuck.activeSelf && !platformChuck.activeSelf) {
				platformChuck.SetActive (true);
				platformHeight += 2f;
				platformChuck.transform.position = new Vector3 (ranXpos, platformHeight, 0);
				spikeAppeared = false;
			} else {
				for (int i = 0; i < poolSize; i++) {
					if (!platformSpikePool [i].activeSelf) {
						platformSpikePool [i].SetActive (true);
						platformHeight += 2f;
						platformSpikePool [i].transform.position = new Vector3 (ranXpos, platformHeight, 0);
						spikeAppeared = true;
						break;
					}
				}
			}
		}

	}

	public void InitPlatform () {

		platformChuck.SetActive (false);
		for (int i = 0; i < poolSize; i++) {
			platformRegularPool [i].SetActive (false);
			platformFallPool [i].SetActive (false);
			platformMovePool [i].SetActive (false);
			platformSpikePool [i].SetActive (false);
		}

		platformBase.SetActive (true);

		platformHeight = 0;
		float ranXpos = Random.Range (-2.5f, 2.5f);
		platformRegularPool [0].SetActive (true);
		platformRegularPool [0].transform.position = new Vector3 (ranXpos, platformHeight, 0);

	}

}
