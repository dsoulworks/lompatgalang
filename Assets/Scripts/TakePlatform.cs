﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePlatform : MonoBehaviour {

	void OnTriggerEnter (Collider other) {

		if (other.gameObject.tag == "Platform") {
			other.gameObject.GetComponent<ForceUp> ().touch = 0;
			other.gameObject.SetActive (false);
		}

		if (other.gameObject.tag == "Friend") {
			other.gameObject.SetActive (false);
		}

	}

}
